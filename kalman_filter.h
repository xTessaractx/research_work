/****************************************************************************
****************************************************************************
		File Name: kalman_filter.h
		Programmer: Tessa Herzberger
		Created: February 2020
		Last Modified: 15 February 2020
****************************************************************************
****************************************************************************/

#ifndef KALMAN_FILTER
#define KALMAN_FILTER

double kalman_filter(double samples[], double prediction[], double probability_array[], double new_samples[]);
double kalman_update_average(double, double, double, double);
double kalman_update_variance(double, double);
double kalman_predict_average(double, double);
double kalman_predict_variance(double, double);
double kalman_prob_dens_func_dataPoint(double, double, double);
double kalman_prediction(double probability_array[], double samples[]);
void perform_kalman_filter(double samples[], double probability_array[], double predicted_vals[], double new_samples[]);
//void removing_values_via_kalman_filter(double samples[], double prob_array[], double new_samples[]);

#endif KALMAN_FILTER
