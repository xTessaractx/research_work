/****************************************************************************
****************************************************************************
		File Name: rssi.h
		Programmer: Tessa Herzberger
		Created: January 2020
		Last Modified: March 2020

		Copywrite Tessa Herzberger, 2020
		For use only with permission from Tessa Herzberger.
		Please contact Tessa at tessaherzberger@gmail.com to use code.
****************************************************************************
****************************************************************************/

#ifndef RSSI_H
#define RSSI_H

double convert_RSSI_to_distance(double rssi);
void trilateration_distances(double predicted_rssi_val[], double distances[]);

#endif RSSI_H