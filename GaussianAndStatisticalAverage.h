/****************************************************************************
****************************************************************************
		File Name: GaussianAndStatisticalAverage.h
		Programmer: Tessa Herzberger
		Created: February 2020
		Last Modified: March 2020

		Copywrite Tessa Herzberger, 2020
		For use only with permission from Tessa Herzberger.
		Please contact Tessa at tessaherzberger@gmail.com to use code.
****************************************************************************
****************************************************************************/

#ifndef GAUSSIAN_AND_STATISTICAL_AVERAGE_H
#define GAUSSIAN_AND_STATISTICAL_AVERAGE_H

double gauss_dist_and_statistical_avg_filter(double probability_array[], double samples[], double new_samples[]);
void perform_gaussian_and_stat_avg_filter(double samples[], double probability_array[], double new_samples[], double predicted_vals[]);
void underground_perform_gaussian_and_stat_avg_filter(double samples[], double probability_array[], double new_samples[], double predicted_vals[]);

#endif GAUSSIAN_AND_STATISTICAL_AVERAGE_H
