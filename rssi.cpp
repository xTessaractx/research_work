#include "rssi.h"
#include "global_constants.h"
#include "statistical_avg.h"
#include "gaussian_dist.h"
#include "GaussianAndStatisticalAverage.h"
#include "math.h"


/****************************************************************************
		Function Name: convert_RSSI_to_distance
		Programmer: Tessa Herzberger
		Parameters taken: Double
		Parameter returned: Double
		Program Description: Calculates the distance value for a given
							 RSSI value. This distance value is returned.
****************************************************************************/
double convert_RSSI_to_distance(double rssi)
{
	double distance = 0;
	double temp0 = 0;
	double temp1 = 0;


	//for distance measurements
//	temp0 = (rssi - (A_constant_basement_baseline));
	//for trilateration measurements
//	temp0 = (rssi - (A_constant_trilateration));
	//For 2.4GH measurements
//	temp0 = (rssi - (A_constant_2_4_GHz));
	//For underground measurements
	temp0 = (rssi - (A_constant_underground));

	//for distance measurements
//	temp1 = (-10 * n_constant_basement_baseline);
	//for trilateration measurements
//	temp1 = (-10 * n_constant_trilateration);
	//For 2.4GH measurements
//	temp1 = (-10 * n_constant_2_4_GHz);
	//For underground measurements
	temp1 = (-10 * n_constant_underground);

	temp0 = temp0 / temp1;
	distance = pow(10, temp0);


	return distance;
}


/****************************************************************************
		Function Name: trilateration_distances
		Programmer: Tessa Herzberger
		Parameters taken: Double array, double array
		Parameter returned: Nothing
		Program Description: Calculates the distance values for given
							 RSSI values for an array of predicted RSSI
							 vales. This is done by calling the
							 convert_RSSI_to_distance function.
****************************************************************************/
void trilateration_distances(double predicted_rssi_val[], double distances[])
{
	for (int i = 0; i < MAX_SAMPLES; i++)
	{
		if (predicted_rssi_val[i] != DEAD_BEEF_VAL)
			distances[i] = convert_RSSI_to_distance(predicted_rssi_val[i]);
		else
			distances[i] = DEAD_BEEF_VAL;
	}

	return;
}
