/****************************************************************************
****************************************************************************
		File Name: GaussianAndStatisticalAverage.cpp
		Programmer: Tessa Herzberger
		Functions within the File:
					- gauss_dist_and_statistical_avg_filter
					- perform_gaussian_and_stat_avg_filter
		Created: January 2020
		Last Modified: March 2020

		Copywrite Tessa Herzberger, 2020
		For use only with permission from Tessa Herzberger.
		Please contact Tessa at tessaherzberger@gmail.com to use code.
****************************************************************************
****************************************************************************/


#include "gaussian_dist.h"
#include "global_constants.h"
#include "statistical_avg.h"
#include "GaussianAndStatisticalAverage.h"
//#include <iomanip>
#include <iostream>
#include <cmath>

using namespace std;

/****************************************************************************
		Function Name: gauss_dist_and_statistical_avg_filter
		Programmer: Tessa Herzberger
		Parameters taken: Double array, double array, double array
		Parameter returned: Double
		Program Description: Calculates the average and standard
							 deviation for a given sample set using
							 calc_average and calc_std_deviation
							 functions respectively. Next, the probability
							 for each data point is calculated using the
							 prob_dens_func_dataPoint function. After
							 that, the gaussian_prediction function is
							 called to determine the most likely sample.
							 The most likely sample is used to filter out
							 values that are more than one standard
							 deviation away from the most likely sample.
							 The statistical average of the newly filtered
							 sample set is calculated using the
							 calc_stat_avg function. This average value
							 is returned.
****************************************************************************/
double gauss_dist_and_statistical_avg_filter(double probability_array[], double samples[], double new_samples[])
{
	double probability_value = 0;
	double average = 0.0;
	double std_dev = 0.0;
	double most_likely_sample = 0.0;
	double predicted_value = 0.0;

	average = calc_average(samples);
	std_dev = calc_std_deviation(average, samples);

	for (int i = 0; i < MAX_SAMPLES; i++)
	{
		probability_array[i] = prob_dens_func_dataPoint(samples[i], average, std_dev);
		if (isnan(probability_array[i]))
			probability_array[i] = 100;
	}

	most_likely_sample = gaussian_prediction(probability_array, samples);

	for (int i = 0; i < MAX_SAMPLES; i++)
	{
		if (((samples[i] >(most_likely_sample + (std_dev * 2.0))) || (samples[i] < (most_likely_sample - (std_dev * 2.0)))) && (probability_array[i] != 100) && (samples[i] != DEAD_BEEF_VAL))
			new_samples[i] = DEAD_BEEF_VAL;
		else
			new_samples[i] = samples[i];
	}

	predicted_value = calc_stat_avg(new_samples);

	return predicted_value;
}

/****************************************************************************
		Function Name: perform_gaussian_and_stat_avg_filter
		Programmer: Tessa Herzberger
		Parameters taken: Double array, double array, double array,
						  double array
		Parameter returned: Nothing
		Program Description: Performs the Gaussian And Statistical
							 Average Filter for a set of data
							 points at various distances. This is
							 done by defining the samples array
							 (which contains the set of data points),
							 then calling the
							 gauss_dist_and_statistical_avg_filter
							 function for each set of data points.
****************************************************************************/
void perform_gaussian_and_stat_avg_filter(double samples[], double probability_array[], double new_samples[], double predicted_vals[])
{
	for (int i = 0; i < MAX_METERS; i++)
	{
		if (i == 0)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = ZERO_METERS_BASELINE[j];
		}
		if (i == 1)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = ONE_METER_2_4GHZ[j];
				//samples[j] = ONE_METER_BASELINE[j];
		}
		if (i == 2)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TWO_METER_2_4GHZ[j];
				//samples[j] = TWO_METERS_BASELINE[j];
		}
		if (i == 3)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = THREE_METER_2_4GHZ[j];
				//samples[j] = THREE_METERS_BASELINE[j];
		}
		if (i == 4)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = FOUR_METER_2_4GHZ[j];
				//samples[j] = FOUR_METERS_BASELINE[j];
		}
		if (i == 5)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = FIVE_METER_2_4GHZ[j];
				//samples[j] = FIVE_METERS_BASELINE[j];
		}
		if (i == 6)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = SIX_METER_2_4GHZ[j];
				//samples[j] = SIX_METERS_BASELINE[j];
		}
		if (i == 7)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = SEVEN_METER_2_4GHZ[j];
				//samples[j] = SEVEN_METERS_BASELINE[j];
		}
		if (i == 8)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = EIGHT_METER_2_4GHZ[j];
				//samples[j] = EIGHT_METERS_BASELINE[j];
		}
		if (i == 9)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = NINE_METER_2_4GHZ[j];
				//samples[j] = NINE_METERS_BASELINE[j];
		}
		if (i == 10)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TEN_METER_2_4GHZ[j];
				//samples[j] = TEN_METERS_BASELINE[j];
		}
		if (i == 15)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = FIFTEEN_METERS_BASELINE[j];
		}
		if (i == 20)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TWENTY_METERS_BASELINE[j];
		}
		if (i == 25)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TWENTY_FIVE_METERS_BASELINE[j];
		}
		if (i == 30)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = THIRTY_METERS_BASELINE[j];
		}

		if (i == 0 || i == 1 || i == 2 || i == 3 || i == 4 || i == 5 || i == 6 || i == 7 || i == 8 ||
			i == 9 || i == 10 || i == 15 || i == 20 || i == 25 || i == 30)
		{
			predicted_vals[i] = gauss_dist_and_statistical_avg_filter(probability_array, samples, new_samples);
			cout << "Predicted value for " << i << " meters = " << predicted_vals[i] << endl;
		}
		else
			predicted_vals[i] = DEAD_BEEF_VAL;
	}

}


/****************************************************************************
Function Name: perform_gaussian_and_stat_avg_filter
Programmer: Tessa Herzberger
Parameters taken: Double array, double array, double array,
double array
Parameter returned: Nothing
Program Description: Performs the Gaussian And Statistical
Average Filter for a set of data
points at various distances. This is
done by defining the samples array
(which contains the set of data points),
then calling the
gauss_dist_and_statistical_avg_filter
function for each set of data points.
****************************************************************************/
void underground_perform_gaussian_and_stat_avg_filter(double samples[], double probability_array[], double new_samples[], double predicted_vals[])
{
	for (int i = 0; i < MAX_METERS; i++)
	{
		if (i == 0)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = ZERO_METERS_UNDERGROUND[j];
		}
		if (i == 1)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = ONE_METER_UNDERGROUND[j];
			//samples[j] = ONE_METER_BASELINE[j];
		}
		if (i == 2)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TWO_METERS_UNDERGROUND[j];
			//samples[j] = TWO_METERS_BASELINE[j];
		}
		if (i == 3)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = THREE_METERS_UNDERGROUND[j];
			//samples[j] = THREE_METERS_BASELINE[j];
		}
		if (i == 4)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = FOUR_METERS_UNDERGROUND[j];
			//samples[j] = FOUR_METERS_BASELINE[j];
		}
		if (i == 5)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = FIVE_METERS_UNDERGROUND[j];
			//samples[j] = FIVE_METERS_BASELINE[j];
		}
		if (i == 6)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = SIX_METERS_UNDERGROUND[j];
			//samples[j] = SIX_METERS_BASELINE[j];
		}
		if (i == 7)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = SEVEN_METERS_UNDERGROUND[j];
			//samples[j] = SEVEN_METERS_BASELINE[j];
		}
		if (i == 8)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = EIGHT_METERS_UNDERGROUND[j];
			//samples[j] = EIGHT_METERS_BASELINE[j];
		}
		if (i == 9)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = NINE_METERS_UNDERGROUND[j];
			//samples[j] = NINE_METERS_BASELINE[j];
		}
		if (i == 10)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TEN_METERS_UNDERGROUND[j];
			//samples[j] = TEN_METERS_BASELINE[j];
		}
		if (i == 15)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = FIFTEEN_METERS_UNDERGROUND[j];
		}
		if (i == 20)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TWENTY_METERS_UNDERGROUND[j];
		}
		if (i == 25)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TWENTY_FIVE_METERS_UNDERGROUND[j];
		}
		if (i == 30)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = THIRTY_METERS_UNDERGROUND[j];
		}

		if (i == 0 || i == 1 || i == 2 || i == 3 || i == 4 || i == 5 || i == 6 || i == 7 || i == 8 ||
			i == 9 || i == 10 || i == 15 || i == 20 || i == 25 || i == 30)
		{
			predicted_vals[i] = gauss_dist_and_statistical_avg_filter(probability_array, samples, new_samples);
			cout << "Predicted value for " << i << " meters = " << predicted_vals[i] << endl;
		}
		else
			predicted_vals[i] = DEAD_BEEF_VAL;
	}

}
