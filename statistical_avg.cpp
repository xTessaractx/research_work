/****************************************************************************
****************************************************************************
		File Name: statistical_avg.cpp
		Programmer: Tessa Herzberger
		Functions within the File:
					- calc_stat_avg
					- perform_statistical_average
		Created: January 2020
		Last Modified: March 2020

		Copywrite Tessa Herzberger, 2020
		For use only with permission from Tessa Herzberger.
		Please contact Tessa at tessaherzberger@gmail.com to use code.
****************************************************************************
****************************************************************************/

#include "statistical_avg.h"
#include "global_constants.h"
#include <iostream>

using namespace std;


/****************************************************************************
		Function Name: calc_stat_avg
		Programmer: Tessa Herzberger
		Parameters taken: double array
		Parameter returned: double
		Program Description: Calculates the statistical average for a
							 given array.
****************************************************************************/
double calc_stat_avg(double samples[])
{
	double stat_avg = 0;
	double sum = 0;
	double counter = 0;

	for (int i = 0; i < MAX_SAMPLES; i++)
	{
		if (samples[i] != DEAD_BEEF_VAL)
		{
			sum += samples[i];
			counter++;
		}
	}

	stat_avg = (sum / counter);
	
	return stat_avg;
}

/****************************************************************************
		Function Name: perform_statistical_average
		Programmer: Tessa Herzberger
		Parameters taken: Double array, double array
		Parameter returned: Double
		Program Description: Performs the Statistical Average Filter
							 for a set of data points at various distances.
							 This is done by defining the samples array
							 (which contains the set of data points),
							 then calling the calc_stat_avg
							 function for each set of data points.
****************************************************************************/
void perform_statistical_average(double samples[], double predicted_vals[])
{
	for (int i = 0; i < MAX_METERS; i++)
	{
		if (i == 0)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				//samples[j] = ZERO_METER_2_4GHZ[j];
				samples[j] = ZERO_METERS_BASELINE[j];
		}
		if (i == 1)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = ONE_METER_2_4GHZ[j];
				//samples[j] = ONE_METER_BASELINE[j];
		}
		if (i == 2)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TWO_METER_2_4GHZ[j];
				//samples[j] = TWO_METERS_BASELINE[j];
		}
		if (i == 3)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = THREE_METER_2_4GHZ[j];
				//samples[j] = THREE_METERS_BASELINE[j];
		}
		if (i == 4)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = FOUR_METER_2_4GHZ[j];
				//samples[j] = FOUR_METERS_BASELINE[j];
		}
		if (i == 5)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = FIVE_METER_2_4GHZ[j];
				//samples[j] = FIVE_METERS_BASELINE[j];
		}
		if (i == 6)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = SIX_METER_2_4GHZ[j];
				//samples[j] = SIX_METERS_BASELINE[j];
		}
		if (i == 7)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = SEVEN_METER_2_4GHZ[j];
				//samples[j] = SEVEN_METERS_BASELINE[j];
		}
		if (i == 8)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = EIGHT_METER_2_4GHZ[j];
				//samples[j] = EIGHT_METERS_BASELINE[j];
		}
		if (i == 9)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = NINE_METER_2_4GHZ[j];
				//samples[j] = NINE_METERS_BASELINE[j];
		}
		if (i == 10)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TEN_METER_2_4GHZ[j];
				//samples[j] = TEN_METERS_BASELINE[j];
		}
		if (i == 15)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = FIFTEEN_METERS_BASELINE[j];
		}
		if (i == 20)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TWENTY_METERS_BASELINE[j];
		}
		if (i == 25)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TWENTY_FIVE_METERS_BASELINE[j];
		}
		if (i == 30)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = THIRTY_METERS_BASELINE[j];
		}

		if (i == 0 || i == 1 || i == 2 || i == 3 || i == 4 || i == 5 || i == 6 || i == 7 || i == 8 ||
			i == 9 || i == 10 || i == 15 || i == 20 || i == 25 || i == 30)
		{
			predicted_vals[i] = calc_stat_avg(samples);
			cout << "Predicted value for " << i << " meters = " << predicted_vals[i] << endl;
		}
		else
			predicted_vals[i] = DEAD_BEEF_VAL;
	}

}




/****************************************************************************
Function Name: perform_statistical_average
Programmer: Tessa Herzberger
Parameters taken: Double array, double array
Parameter returned: Double
Program Description: Performs the Statistical Average Filter
for a set of data points at various distances.
This is done by defining the samples array
(which contains the set of data points),
then calling the calc_stat_avg
function for each set of data points.
****************************************************************************/
void underground_perform_statistical_average(double samples[], double predicted_vals[])
{
	for (int i = 0; i < MAX_METERS; i++)
	{
		if (i == 0)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				//samples[j] = ZERO_METER_2_4GHZ[j];
				samples[j] = ZERO_METERS_UNDERGROUND[j];
		}
		if (i == 1)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = ONE_METER_UNDERGROUND[j];
			//samples[j] = ONE_METER_BASELINE[j];
		}
		if (i == 2)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TWO_METERS_UNDERGROUND[j];
			//samples[j] = TWO_METERS_BASELINE[j];
		}
		if (i == 3)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = THREE_METERS_UNDERGROUND[j];
			//samples[j] = THREE_METERS_BASELINE[j];
		}
		if (i == 4)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = FOUR_METERS_UNDERGROUND[j];
			//samples[j] = FOUR_METERS_BASELINE[j];
		}
		if (i == 5)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = FIVE_METERS_UNDERGROUND[j];
			//samples[j] = FIVE_METERS_BASELINE[j];
		}
		if (i == 6)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = SIX_METERS_UNDERGROUND[j];
			//samples[j] = SIX_METERS_BASELINE[j];
		}
		if (i == 7)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = SEVEN_METERS_UNDERGROUND[j];
			//samples[j] = SEVEN_METERS_BASELINE[j];
		}
		if (i == 8)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = EIGHT_METERS_UNDERGROUND[j];
			//samples[j] = EIGHT_METERS_BASELINE[j];
		}
		if (i == 9)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = NINE_METERS_UNDERGROUND[j];
			//samples[j] = NINE_METERS_BASELINE[j];
		}
		if (i == 10)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TEN_METERS_UNDERGROUND[j];
			//samples[j] = TEN_METERS_BASELINE[j];
		}
		if (i == 15)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = FIFTEEN_METERS_UNDERGROUND[j];
		}
		if (i == 20)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TWENTY_METERS_UNDERGROUND[j];
		}
		if (i == 25)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = TWENTY_FIVE_METERS_UNDERGROUND[j];
		}
		if (i == 30)
		{
			for (int j = 0; j < MAX_SAMPLES; j++)
				samples[j] = THIRTY_METERS_UNDERGROUND[j];
		}

		if (i == 0 || i == 1 || i == 2 || i == 3 || i == 4 || i == 5 || i == 6 || i == 7 || i == 8 ||
			i == 9 || i == 10 || i == 15 || i == 20 || i == 25 || i == 30)
		{
			predicted_vals[i] = calc_stat_avg(samples);
			cout << "Predicted value for " << i << " meters = " << predicted_vals[i] << endl;
		}
		else
			predicted_vals[i] = DEAD_BEEF_VAL;
	}

}