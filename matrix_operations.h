/****************************************************************************
****************************************************************************
		File Name: matrix_operations.h
		Programmer: Tessa Herzberger
		Created: March 2020
		Last Modified: March 2020

		Copywrite Tessa Herzberger, 2020
		For use only with permission from Tessa Herzberger.
		Please contact Tessa at tessaherzberger@gmail.com to use code.
****************************************************************************
****************************************************************************/

#ifndef MATRIX_OPERATIONS_H
#define MATRIX_OPERATIONS_H

void inverse_matrix(double matrix[][3], double node1[], double node2[], double node3[], double node4[]);
void construct_trilateration_vector(double vector[3], double node1[], double node2[], double node3[], double node4[], double rssi_values[]);
void multiply_matrix_by_vector(double result[], double matrix[][3], double vector[]);

#endif MATRIX_OPERATIONS_H