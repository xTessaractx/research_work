/****************************************************************************
****************************************************************************
		File Name: Trilateration_Filtering.h
		Programmer: Tessa Herzberger
		Created: February 2020
		Last Modified: March 2020

		Copywrite Tessa Herzberger, 2020
		For use only with permission from Tessa Herzberger.
		Please contact Tessa at tessaherzberger@gmail.com to use code.
****************************************************************************
****************************************************************************/

#ifndef TRILATERATION_FILTERING_H
#define TRILATERATION_FILTERING_H

double trilateration_filtered_rssi(double samples[], double filter_type);

#endif TRILATERATION_FILTERING_H