/****************************************************************************
****************************************************************************
		File Name: main.cpp
		Programmers: Tessa Herzberger
		Functions within the File:
					- main
		Created: January 2020
		Last Modified: March 2020
		
		Copywrite Tessa Herzberger, 2020
		For use only with permission from Tessa Herzberger.
		Please contact Tessa at tessaherzberger@gmail.com to use code.
****************************************************************************
****************************************************************************/


#include "gaussian_dist.h"
#include "statistical_avg.h"
#include "global_constants.h"
#include "kalman_filter.h"
#include "GaussianAndStatisticalAverage.h"
#include "rssi.h"
#include "Trilateration_Filtering.h"
#include "matrix_operations.h"
#include "Error_Calculations.h"
//#include <iomanip>
#include <iostream>

int main()
{
	double unknown_node_location[M - 1];
	double trilateration_vector[3];
	double distance_values[M];
	double rssi_values[M] = {};
	double inv_matrix[3][3];

	double filter_type;
	std::cout << "What filter would you like to use?" << std::endl << " 1 - Gaussian Stat Avg" << std::endl << " 2 - Gaussian" << std::endl << " 3 - Stat Avg" << std::endl;
	std::cin >> filter_type;

	//Trilateration: Large Distance. Unknown Node Location = (8.5, 8.5, 0.0)
//	double node0[M - 1] = { 4.5, 6.5, 0 };
//	double node1[M - 1] = { 12, 4, 0 };
//	double node2[M - 1] = { 9, 33, 0 };
//	double node3[M - 1] = { 8.5, 8.5, 2 };

	//Trilateration: Small Distance. Unknown Node Location = (8.5, 8.5, 0.0)
	double node0[M - 1] = { 8.5, 8.5, 1 };
	double node1[M - 1] = { 10, 7.5, 0 };
	double node2[M - 1] = { 6.5, 7, 0 };
	double node3[M - 1] = { 8.5, 7.08, 1.42 };

	//Trilateration: Nick's values. Unknown Node Location = (12, 12, 4)
//		double node0[M-1] = { 0, 0, 0 };
//		double node1[M-1] = { 24, 12, 0 };
//		double node2[M-1] = { 24, 24, 8 };
//		double node3[M-1] = { 12, 24, 8 };


	inverse_matrix(inv_matrix, node0, node1, node2, node3);
	std::cout << "Main Inv Matrix Row 1 = " << inv_matrix[0][0] << ", " << inv_matrix[0][1] << ", " << inv_matrix[0][2] << std::endl;
	std::cout << "Main Inv Matrix Row 2 = " << inv_matrix[1][0] << ", " << inv_matrix[1][1] << ", " << inv_matrix[1][2] << std::endl;
	std::cout << "Main Inv Matrix Row 3 = " << inv_matrix[2][0] << ", " << inv_matrix[2][1] << ", " << inv_matrix[2][2] << std::endl;


	// VVVV TESSA - DETERMINE RSSI VALUES VVVV
	double sample_1_meter_test[MAX_SAMPLES] = {};
	for (int i = 0; i < MAX_SAMPLES; i++)
		sample_1_meter_test[i] = TRILATERATION_1_METER[i];
	rssi_values[0] = trilateration_filtered_rssi(sample_1_meter_test, filter_type);
	std::cout << "TESTING 1 METER TRILATERATION_FILTERED_RSSI " << rssi_values[0] << std::endl;

	double sample_1_8_meter_test[MAX_SAMPLES] = {};
	for (int i = 0; i < MAX_SAMPLES; i++)
		sample_1_8_meter_test[i] = TRILATERATION_1_8_METERS[i];
	rssi_values[1] = trilateration_filtered_rssi(sample_1_8_meter_test, filter_type);
	std::cout << "TESTING 1.8 METER TRILATERATION_FILTERED_RSSI " << rssi_values[1] << std::endl;

	double sample_2_5_meter_test[MAX_SAMPLES] = {};
	for (int i = 0; i < MAX_SAMPLES; i++)
		sample_2_5_meter_test[i] = TRILATERATION_2_5_METERS[i];
	rssi_values[2] = trilateration_filtered_rssi(sample_2_5_meter_test, filter_type);
	std::cout << "TESTING 2.5 METER TRILATERATION_FILTERED_RSSI " << rssi_values[2] << std::endl;

	double sample_2_meter_test[MAX_SAMPLES] = {};
	for (int i = 0; i < MAX_SAMPLES; i++)
		sample_2_meter_test[i] = TRILATERATION_2_METERS[i];
	rssi_values[3] = trilateration_filtered_rssi(sample_2_meter_test, filter_type);
	std::cout << "TESTING 2 METER TRILATERATION_FILTERED_RSSI " << rssi_values[3] << std::endl;
	// ^^^^ TESSA - DETERMINE RSSI VALUES ^^^^

	// Calculated RSSI Values for small distance trilateration
	//	rssi_values[0] = -29; //1 meter
	//	rssi_values[1] = -39.2; ////1.8 meters
	//	rssi_values[2] = -44.9; //2.5 meters
	//	rssi_values[3] = -41; //2 meters

	// Unfiltered RSSI Values for small distance trilateration
	//	rssi_values[0] = -28; //1 meter
	//	rssi_values[1] = -38; //1.8 meters
	///	rssi_values[2] = -41; //2.5 meters
	//	rssi_values[3] = -40; //2 meters

	// Calculated RSSI Values for large distance trilateration
	//	rssi_values[0] = -55.01;
	//	rssi_values[1] = -59.235;
	//	rssi_values[2] = -84.5667;
	//	rssi_values[3] = -41.06;


	// Calculated RSSI Values for Nick's trilateration
	//	rssi_values[0] = -78.62;
	//	rssi_values[1] = -72.17;
	//	rssi_values[2] = -78.62;
	//	rssi_values[3] = -73.01;

	std::cout << "RSSI VALS = " << rssi_values[0] << ", " << rssi_values[1] << ", " << rssi_values[2] << ", " << rssi_values[3] << std::endl << std::endl;

	for (int i = 0; i < M; i++)
		distance_values[i] = convert_RSSI_to_distance(rssi_values[i]);

	std::cout << "DISTANCE VALS = " << distance_values[0] << ", " << distance_values[1] << ", " << distance_values[2] << ", " << distance_values[3] << std::endl << std::endl;

	construct_trilateration_vector(trilateration_vector, node0, node1, node2, node3, distance_values);

	std::cout << "VECTOR[0] = " << trilateration_vector[0] << std::endl;
	std::cout << "VECTOR[1] = " << trilateration_vector[1] << std::endl;
	std::cout << "VECTOR[2] = " << trilateration_vector[2] << std::endl;

	multiply_matrix_by_vector(unknown_node_location, inv_matrix, trilateration_vector);
	std::cout << "UNKNOWN NODE LOCATION = " << unknown_node_location[0] << ", " << unknown_node_location[1] << ", " << unknown_node_location[2] << std::endl;

	double theoretical_location[M - 1] = { 8.50233, 8.48513, 0.0113218 };
	double absolute_errors[M - 1] = {};
	double radius_error = 0;

	absolute_error_calculations_x_y_z(theoretical_location, unknown_node_location, absolute_errors);
	radius_error = radius_error_calculations(absolute_errors);

	std::cout << "RADIUS ERROR = " << radius_error << std::endl;




	double samples[MAX_SAMPLES] = {};

	//TestBench for Unfiltered Data
	std::cout << std::endl << std::endl << "****************** UNFILTERED FILTER DATA ****************** " << std::endl << std::endl;
	double unfiltered_data[MAX_SAMPLES] = { -8, -34, -40, -42, -44, -42, -50, -60, -44, -47, -47, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
	double unfiltered_distance[MAX_SAMPLES];

	for (int i = 0; i < MAX_SAMPLES; i++)
		if (unfiltered_data[i] != DEAD_BEEF_VAL)
			std::cout << "Predicted RSSI value for " << i << " meters = " << unfiltered_data[i] << std::endl;

	trilateration_distances(unfiltered_data, unfiltered_distance);

	for (int i = 0; i < MAX_SAMPLES; i++)
		if (unfiltered_distance[i] != DEAD_BEEF_VAL)
			std::cout << "Predicted distance value for " << i << " meters = " << unfiltered_distance[i] << std::endl;



	//TestBench for Unfiltered Data
	std::cout << std::endl << std::endl << "****************** UNDERGROUND UNFILTERED FILTER DATA ****************** " << std::endl << std::endl;
	// 1 double underground_unfiltered_data[MAX_SAMPLES] = { -8, -32, -41, -46, -46, -50, -56, -50, -53, -57, -52, DEAD_BEEF_VAL, DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL , DEAD_BEEF_VAL };
	// 2 double underground_unfiltered_data[MAX_SAMPLES] = { -8, -32, -40, -44, -46, -50, -53, -50, -54, -57, -51, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
	// 3 double underground_unfiltered_data[MAX_SAMPLES] = { -8, -33, -40, -45, -45, -52, -51, -51, -53, -64, -53, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
	// 4 double underground_unfiltered_data[MAX_SAMPLES] = { -8, -34, -40, -46, -46, -52, -53, -52, -55, -57, -52, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
	// 5 double underground_unfiltered_data[MAX_SAMPLES] = { -8, -34, -40, -45, -47, -53, -51, -52, -53, -62,  -52, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
	// 6 double underground_unfiltered_data[MAX_SAMPLES] = { -8, -34, -40, -46, -47, -51, -53, -51, -51, -57, -52, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
	// 7 double underground_unfiltered_data[MAX_SAMPLES] = { -8, -33, -39, -45, -46, -52, -53, -49, -52, -56,  -52, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
	// 8 double underground_unfiltered_data[MAX_SAMPLES] = { -8, -34, -39, -45, -45, -53, -53, -51, -50, -61, -52, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
	// 9 double underground_unfiltered_data[MAX_SAMPLES] = { -8, -34, -40, -46, -46, -54, -54, -51, -52, -56, -52, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
	//10 double underground_unfiltered_data[MAX_SAMPLES] = { -8, -34, -41, -46, -46, -50, -55, -52, -54, -58, -52, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
	
	// max error sample 
	double underground_unfiltered_data[MAX_SAMPLES] = { -8, -32, -39, -49, -49, -54, -56, -48, -50, -60, -50, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
	// double underground_unfiltered_data[MAX_SAMPLES] = { -8, -34, -39, -45, -45, -53, -53, -51, -50, -61, -52, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
	double underground_unfiltered_distance[MAX_SAMPLES];

	for (int i = 0; i < MAX_SAMPLES; i++)
		if (underground_unfiltered_data[i] != DEAD_BEEF_VAL)
			std::cout << "Predicted RSSI value for " << i << " meters = " << underground_unfiltered_data[i] << std::endl;

	trilateration_distances(underground_unfiltered_data, underground_unfiltered_distance);

	for (int i = 0; i < MAX_SAMPLES; i++)
		if (underground_unfiltered_distance[i] != DEAD_BEEF_VAL)
			std::cout << "Predicted distance value for " << i << " meters = " << underground_unfiltered_distance[i] << std::endl;





	//TestBench for Statistical Average Filter Data
	std::cout << std::endl << std::endl << "****************** STATISTICAL AVERAGE FILTER DATA ****************** " << std::endl << std::endl;
	double predicted_val_stat_avg[MAX_SAMPLES];
	double stat_avg_distance[MAX_SAMPLES];

	underground_perform_statistical_average(samples, predicted_val_stat_avg);
	trilateration_distances(predicted_val_stat_avg, stat_avg_distance);

	for (int i = 0; i < MAX_SAMPLES; i++)
		if (predicted_val_stat_avg[i] != DEAD_BEEF_VAL)
			std::cout << "Predicted distance value for " << i << " meters = " << stat_avg_distance[i] << std::endl;


	//TestBench for Underground Statistical Average Filter Data
	std::cout << std::endl << std::endl << "****************** UNDERGROUND STATISTICAL AVERAGE FILTER DATA ****************** " << std::endl << std::endl;
	double underground_stat_avg_samples[MAX_SAMPLES] = {};
	double underground_predicted_val_stat_avg[MAX_SAMPLES];
	double underground_stat_avg_distance[MAX_SAMPLES];
	double underground_stat_avg_absolute_error[MAX_SAMPLES];
	double underground_stat_avg_theoretical_values[MAX_SAMPLES] = { 0, 1.02, 1.98, 2.96, 3.97, 4.96, 6, 6.97, 7.97, 9, 9.99, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, 15.02, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };

	underground_perform_statistical_average(underground_stat_avg_samples, underground_predicted_val_stat_avg);
	trilateration_distances(underground_predicted_val_stat_avg, underground_stat_avg_distance);

	for (int i = 0; i < MAX_SAMPLES; i++)
		if (underground_predicted_val_stat_avg[i] != DEAD_BEEF_VAL)
			std::cout << "Predicted distance value for " << i << " meters = " << underground_stat_avg_distance[i] << std::endl;

	absolute_error_calculations(underground_stat_avg_theoretical_values, underground_stat_avg_distance, underground_stat_avg_absolute_error);

	for (int i = 0; i < MAX_SAMPLES; i++)
		std::cout << "Absolute Error Value " << i << " meters = " << underground_stat_avg_absolute_error[i] << std::endl;


	

	//TestBench for Gaussian Filter Data
	std::cout << std::endl << std::endl << "****************** GAUSSIAN FILTER DATA ****************** " << std::endl << std::endl;
	double new_samples_gaussian[MAX_SAMPLES] = {};
	double predicted_val_gaussian[MAX_SAMPLES] = {};
	double prob_array_gaussian[MAX_SAMPLES] = {};
	double gaussian_distance[MAX_SAMPLES];

	perform_gaussian_filter(samples, prob_array_gaussian, new_samples_gaussian, predicted_val_gaussian);
	trilateration_distances(predicted_val_gaussian, gaussian_distance);

	for (int i = 0; i < MAX_SAMPLES; i++)
		if (predicted_val_gaussian[i] != DEAD_BEEF_VAL)
			std::cout << "Predicted distance value for " << i << " meters = " << gaussian_distance[i] << std::endl;

	//TestBench for UNDERGROUND Gaussian Filter Data
	std::cout << std::endl << std::endl << "****************** UNDERGROUND GAUSSIAN FILTER DATA ****************** " << std::endl << std::endl;
	double underground_samples[MAX_SAMPLES] = {};
	double underground_new_samples_gaussian[MAX_SAMPLES] = {};
	double underground_predicted_val_gaussian[MAX_SAMPLES] = {};
	double underground_prob_array_gaussian[MAX_SAMPLES] = {};
	double underground_gaussian_distance[MAX_SAMPLES];
	double underground_theoretical_values[MAX_SAMPLES] = { 0, 1.02, 1.98, 2.96, 3.97, 4.96, 6, 6.97, 7.97, 9, 9.99, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, 15.02, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };
	double underground_gaussian_absolute_error[MAX_SAMPLES] = {};

	perform_gaussian_filter_underground(underground_samples, underground_prob_array_gaussian, underground_new_samples_gaussian, underground_predicted_val_gaussian);
	trilateration_distances(underground_predicted_val_gaussian, underground_gaussian_distance);

	for (int i = 0; i < MAX_SAMPLES; i++)
		if (predicted_val_gaussian[i] != DEAD_BEEF_VAL)
			std::cout << "Predicted distance value for " << i << " meters = " << underground_gaussian_distance[i] << std::endl;

	absolute_error_calculations(underground_theoretical_values, underground_gaussian_distance, underground_gaussian_absolute_error);

	for (int i = 0; i < MAX_SAMPLES; i++)
		std::cout << "Absolute Error Value " << i << " meters = " << underground_gaussian_absolute_error[i] << std::endl;


	//TestBench for Gaussian and Statistical Average Filter Data
	std::cout << std::endl << std::endl << "****************** GAUSSIAN STATISTICAL AVERAGE FILTER DATA ****************** " << std::endl << std::endl;
	double new_samples_gaussian_stat_avg[MAX_SAMPLES] = {};
	double predicted_vals_gaussian_stat_avg[MAX_SAMPLES] = {};
	double prob_array_gaussian_stat_avg[MAX_SAMPLES] = {};
	double gaussian_stat_avg_distance[MAX_SAMPLES];

	perform_gaussian_and_stat_avg_filter(samples, prob_array_gaussian_stat_avg, new_samples_gaussian_stat_avg, predicted_vals_gaussian_stat_avg);
	trilateration_distances(predicted_vals_gaussian_stat_avg, gaussian_stat_avg_distance);

	for (int i = 0; i < MAX_SAMPLES; i++)
	{
		if (predicted_vals_gaussian_stat_avg[i] != DEAD_BEEF_VAL)
			std::cout << "Predicted distance value for " << i << " meters = " << gaussian_stat_avg_distance[i] << std::endl;
	}


	//TestBench for Gaussian and Statistical Average Filter Data
	std::cout << std::endl << std::endl << "****************** UNDERGROUND GAUSSIAN STATISTICAL AVERAGE FILTER DATA ****************** " << std::endl << std::endl;
	double underground_new_samples_gaussian_stat_avg[MAX_SAMPLES] = {};
	double underground_predicted_vals_gaussian_stat_avg[MAX_SAMPLES] = {};
	double underground_prob_array_gaussian_stat_avg[MAX_SAMPLES] = {};
	double underground_gaussian_stat_avg_distance[MAX_SAMPLES];
	double underground_gaussian_stat_avg_samples[MAX_SAMPLES] = {};
	double underground_gaussian_stat_avg_absolute_error[MAX_SAMPLES] = {};
	double underground_gaussian_stat_avg_theoretical_values[MAX_SAMPLES] = { 0, 1.02, 1.98, 2.96, 3.97, 4.96, 6, 6.97, 7.97, 9, 9.99, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, 15.02, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL, DEAD_BEEF_VAL };

	underground_perform_gaussian_and_stat_avg_filter(underground_gaussian_stat_avg_samples, underground_prob_array_gaussian_stat_avg, underground_new_samples_gaussian_stat_avg, underground_predicted_vals_gaussian_stat_avg);
	trilateration_distances(underground_predicted_vals_gaussian_stat_avg, underground_gaussian_stat_avg_distance);

	for (int i = 0; i < MAX_SAMPLES; i++)
	{
		if (predicted_vals_gaussian_stat_avg[i] != DEAD_BEEF_VAL)
			std::cout << "Predicted distance value for " << i << " meters = " << underground_gaussian_stat_avg_distance[i] << std::endl;
	}

	absolute_error_calculations(underground_gaussian_stat_avg_theoretical_values, underground_gaussian_stat_avg_distance, underground_gaussian_stat_avg_absolute_error);

	for (int i = 0; i < MAX_SAMPLES; i++)
		std::cout << "Absolute Error Value " << i << " meters = " << underground_gaussian_stat_avg_absolute_error[i] << std::endl;


	
	//TestBench for Kalman Filter
	std::cout << std::endl << std::endl << "****************** KALMAN FILTER DATA ****************** " << std::endl << std::endl;
	double kalman_samples[MAX_SAMPLES] = {};
	//for (int i = 0; i < MAX_SAMPLES; i++)
	//	kalman_samples[i] = ONE_METER_BASELINE[i];
	double kalman_prediction[MAX_SAMPLES] = {};
	//for (int i = 0; i < MAX_SAMPLES; i++)
	//	kalman_prediction[i] = 0.0;
	double kalman_probability_array[MAX_SAMPLES] = {};
	double kalman_new_samples[MAX_SAMPLES] = {};
	//for (int i = 0; i < MAX_SAMPLES; i++)
	//	kalman_probability_array[i] = 0.0;
	perform_kalman_filter(kalman_samples, kalman_probability_array, kalman_prediction, kalman_new_samples);

	return 0;
}
