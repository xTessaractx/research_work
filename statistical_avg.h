/****************************************************************************
****************************************************************************
		File Name: statistical_avg.h
		Programmer: Tessa Herzberger
		Created: January 2020
		Last Modified: March 2020

		Copywrite Tessa Herzberger, 2020
		For use only with permission from Tessa Herzberger.
		Please contact Tessa at tessaherzberger@gmail.com to use code.
****************************************************************************
****************************************************************************/

#ifndef STATISTICAL_AVG_H
#define STATISTICAL_AVG_H


double calc_stat_avg(double samples[]);
void perform_statistical_average(double samples[], double predicted_vals[]);
void underground_perform_statistical_average(double samples[], double predicted_vals[]);

#endif STATISTICAL_AVG_H
